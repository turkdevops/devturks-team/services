/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

var parse = function(headers, body) {

  var service = body.data && body.data.app && body.data.app.name;
  var head = body.data && body.data.slug && body.data.slug.commit.substring(0, 8);
  var current = body.data && body.data.current;

  if(!service || !head || !current) {
    return;
  }

  var message = `Heroku [${service}] deployed ${head}`;
  var icon = 'logo';

  return {
    message: message,
    icon: icon,
    errorLevel: 'normal'
  };
};

module.exports = {
  apiVersion: 1,
  name: 'Heroku',
  parse: parse
};
